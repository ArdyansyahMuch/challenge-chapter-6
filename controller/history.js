const { UserGame , UserGameHistory } = require ("../models")

class HistoryController {
    static async createHistory(req, res, next) {
        try {
            const { id, userId, last_time_play, last_result } = req.body
            const data = await UserGameHistory.create({
                id,
                userId,
                last_time_play,
                last_result
            })
            console.log(req.body)
            console.log(res)
            res.status(201).json(data)
            console.log(data, "===HISTORY CREATED===")
        } catch (error) {
            console.log(error)
        }
    }

    static async getHistories(req, res, next) {
        try {
            const data = await UserGameHistory.findAll({ 
                include: [
                    {
                        model: UserGame
                    }
                ]
            })
            console.log(req.body)
            console.log(res)
            res.status(201).json(data)
            console.log(data, "===ALL HISTORIES DISPLAYED===")
        } catch (error) {
            console.log(error)
        }
    }
}


module.exports = HistoryController