'use strict';

const usergame = require("../models/usergame");

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('UserGameBiodatas', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
        unique: true
      },
      userId: {
        type: Sequelize.INTEGER,
        references : {
          model: {tableName: "UserGames"},
          key: "id",
        },
        onUpdate : "cascade",
        onDelete : "cascade"
      },
      name: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      },
      city: {
        type: Sequelize.STRING
      },
      phone: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('UserGameBiodatas');
  }
};