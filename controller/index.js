const PlayersController = require("./players")
const BiodataController = require("./biodata")
const HistoryController = require("./history")
const MiddlewareLogin = require("../middleware/login")
// const Middleware = require("")



module.exports = { 
    PlayersController,
    BiodataController,
    HistoryController,
    MiddlewareLogin
}