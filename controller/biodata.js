const { UserGame , UserGameBiodata } = require ("../models")
// const userGameBiodata = require ("../models/usergamebiodata")

class BiodataController {
    static async createBiodata(req, res, next) {
        try {
            const { id, userId, name, email, city, phone } = req.body
            const data = await UserGameBiodata.create({
                id,
                userId,
                name, 
                email,
                city,
                phone
            })
            console.log(req.body)
            console.log(res)
            res.status(201).json(data)
            console.log(data, "===BIODATA CREATED===")
        } catch (error) {
            console.log(error)
        }
    }

    static async getBiodatas(req, res, next) {
        try {
            const data = await UserGameBiodata.findAll({ 
                include: [
                    {
                        model: UserGame
                    }
                ]
            })
            console.log(data, "===DATA DISPLAYED ALL===")
            res.status(200).json(data)
        }   catch (error) {
            console.log(error)
        }
    }
}

module.exports = BiodataController