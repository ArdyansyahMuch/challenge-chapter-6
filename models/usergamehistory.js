'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserGameHistory extends Model {
    static associate(models) {
      UserGameHistory.belongsTo(models.UserGame, { 
        foreignKey: 'userId' })
    }
  }
  UserGameHistory.init({
    id: {unique: true, primaryKey: true, type: DataTypes.INTEGER},
    userId: {
      type: DataTypes.INTEGER,
      references: {
        model: {
          tableName: 'UserGames',
        },
        key: 'id',
  }
},
    last_time_play: DataTypes.DATE,
    last_result: DataTypes.STRING
  }, {
    sequelize,
    tableName: 'UserGameHistories',
  });
  return UserGameHistory;
};