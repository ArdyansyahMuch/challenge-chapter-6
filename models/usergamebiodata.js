'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserGameBiodata extends Model {
    static associate (models) {
      UserGameBiodata.belongsTo(models.UserGame, { 
        foreignKey: 'userId' })
    }
    
  }
  UserGameBiodata.init({
    id: {primaryKey: true, type: DataTypes.INTEGER, unique: true},
    userId: {
      type: DataTypes.INTEGER,
      references: {
              model: {
                tableName: 'UserGames',
              },
              key: 'id',
      },
    }
    ,
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    city: DataTypes.STRING,
    phone: DataTypes.INTEGER,
  }, 
  
  {
    sequelize,
    tableName: 'UserGameBiodatas',
  });
  return UserGameBiodata;
};