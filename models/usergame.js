"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class UserGame extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      UserGame.hasOne(models.UserGameBiodata, {
        foreignKey: "userId",
      });

      UserGame.hasOne(models.UserGameHistory, { 
        foreignKey: "userId"})
      
      // define association here
    }
  }
  UserGame.init(
    {
      id: { primaryKey: true, type: DataTypes.INTEGER, unique: true },
      username: DataTypes.STRING,
      password: DataTypes.STRING,
    },
    {
      sequelize,
      tableName: "UserGames",
    }
  );
  return UserGame;
};
