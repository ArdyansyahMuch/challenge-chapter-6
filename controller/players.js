const { UserGame } = require("../models")
const usergame = require("../models/usergame")

class PlayersController {
    static async createPlayer(req, res, next) {
        try {
            const { id, username, password} = req.body
            console.log(req.body)
            const _payload = {
                id,
                username,
                password
            }
        
            let data = await UserGame.create(_payload)

            data = await UserGame.findAll()
        
            let dataArr = data.map(e => e.dataValues)

            res.redirect("/")

        }   catch (error) {
            console.log(error)
        }
    }
    static async getPlayers(req, res, next) {
        try { 
            const data = await UserGame.findAll()
            console.log(data, "===ALL DATA DISPLAYED===")

            let dataArr = data.map(e => e.dataValues)
            console.log(dataArr, "===data Array===")
            res.render('main', {data: dataArr})
        } catch (error) {
            console.log(error)
        }
    }
    static async getPlayer(req, res, next) {
        try {
            const id = req.params.id
            const data = await UserGame.findOne({
                where : {
                    id : id
                }
            })
            console.log(data, "===CHOOSEN DATA DISPLAYED===")
            res.status(200).json(data)
        } catch (error) {
            console.log(error)
        }
    }
    static async editPlayer(req, res, next) {
        try {
            console.log('====edit yang ini====')
            const { username, password} = req.body
            const _payload = {
                username,
                password
            }
            const id = req.params.id
            let data = await UserGame.update(_payload, {
                where: {
                    id
                },
                returning: true        
            })
           // res.status(201).json(data[1][0])
            res.redirect("/")
            
        } catch (error) {
            console.log(error)
        }
    }
    static async deletePlayer(req, res, next) {
        try {
            const id = req.params.id
            const data = await UserGame.destroy({
                where : {
                    id
                }
            })
            console.log("===processed to delete===")
            
            res.redirect('/')

        } catch (error) {
            console.log(error)
        }
    }
    static async editPlayerView(req, res, next) {
        try {
            const id = req.params.id
            let data = await UserGame.findOne({
                where : {
                    id
                }
            })
            data = data.dataValues
            console.log(data, "==DATA EDIT==")
            res.render('edit', { data })
        } catch (error) {
            console.log(error)
        }
    }
    static async addPlayerView(req, res, next) {
        try {
            res.render('add')
        } catch (error) {
            console.log(error)
        }
            
    }
    static async deletePlayerView(req, res, next) {
        try {
            res.render('add')
        } catch (error) {
            console.log(error)
        }
            
    }
    static async homePage (req, res, next) {

        try {
            res.render('home')
        } catch (error) {
            console.log(error)
        }
        
    }
}


module.exports = PlayersController
